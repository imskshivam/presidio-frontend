// src/urls.js

const BASE_URL = "http://13.234.37.152";

export const LOGIN_URL = `${BASE_URL}/auth/email-login`;
export const CREATE_PROPERTY_URL = `${BASE_URL}/properties/create-properties`;
export const MY_POSTS_URL = `${BASE_URL}/properties/my-post`;

// Add more endpoints as needed
