import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "../Styles/Editproperty.css";

const EditProperty = () => {
  const { propertyId } = useParams();
  const [property, setProperty] = useState(null);
  const [formData, setFormData] = useState({
    name: "",
    description: "",
    bedrooms: 0,
    bathrooms: 0,
    size: 0,
    location: "",
    price: 0,
  });

  useEffect(() => {
    const fetchProperty = async () => {
      try {
        // Fetch property details
        const propertyResponse = await fetch(
          `http://13.234.37.152/properties/getbyId?id=${propertyId}`
        );
        const propertyData = await propertyResponse.json();
        setProperty(propertyData.data);

        // Set form data with fetched property details
        setFormData({
          name: propertyData.data.name,
          description: propertyData.data.description,
          bedrooms: propertyData.data.bedrooms,
          bathrooms: propertyData.data.bathrooms,
          size: propertyData.data.size,
          location: propertyData.data.location,
          price: propertyData.data.price,
        });
      } catch (error) {
        console.error("Error fetching property details:", error);
      }
    };

    fetchProperty();
  }, [propertyId]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `http://13.234.37.152/properties/updatepost?id=${propertyId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(formData),
        }
      );

      if (response.ok) {
        console.log("Property details updated successfully.");
      } else {
        console.error("Failed to update property details.");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  if (!property) {
    return <div>Loading...</div>;
  }

  return (
    <div className="edit-property-container">
      <h2>Edit Property</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Name:</label>
          <input
            type="text"
            name="name"
            value={formData.name}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Description:</label>
          <textarea
            name="description"
            value={formData.description}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Bedrooms:</label>
          <input
            type="number"
            name="bedrooms"
            value={formData.bedrooms}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Bathrooms:</label>
          <input
            type="number"
            name="bathrooms"
            value={formData.bathrooms}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Size (sq ft):</label>
          <input
            type="number"
            name="size"
            value={formData.size}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Location:</label>
          <input
            type="text"
            name="location"
            value={formData.location}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label>Price:</label>
          <input
            type="number"
            name="price"
            value={formData.price}
            onChange={handleChange}
          />
        </div>
        <button type="submit">Update Property</button>
      </form>
    </div>
  );
};

export default EditProperty;
