import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../App.css";

const Myproperties = () => {
  const [properties, setProperties] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [user, setUser] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      const parsedUser = JSON.parse(storedUser);
      setUser(parsedUser);
    } else {
      navigate("/login");
    }
  }, [navigate]);

  useEffect(() => {
    if (user) {
      fetchProperties();
    }
  }, [user, page]);

  const fetchProperties = async () => {
    setLoading(true);
    try {
      const res = await fetch(
        `http://13.234.37.152/properties/my-post?id=${user.data._id}&page=${page}`
      );
      const data = await res.json();
      setProperties((prev) => [...prev, ...data.data]);
    } catch (error) {
      console.error("Error fetching properties:", error);
    } finally {
      setLoading(false);
    }
  };

  const handleCardClick = (propertyId) => {
    navigate(`/property/edit/${propertyId}`);
  };

  const handleInfiniteScroll = () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      setPage((prev) => prev + 1);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleInfiniteScroll);
    return () => window.removeEventListener("scroll", handleInfiniteScroll);
  }, []);

  if (!user) {
    return null; // or a loading indicator
  }

  return (
    <div className="property-list">
      {properties.map((property) => (
        <div
          key={property._id}
          className="property-card"  style={{ cursor: "pointer" }}
          onClick={() => handleCardClick(property._id)}
        >
          <div className="image-container">
            <img
              src={property.images[0]}
              alt={property.name}
              className="property-image"
            />
          </div>
          <div className="property-details">
            <div className="property-name">
              <h2>{property.name}</h2>
            </div>
            <div className="property-price">
              <p>${property.price}</p>
            </div>
          </div>
        </div>
      ))}
      {loading && <div>Loading...</div>}
    </div>
  );
};

export default Myproperties;
