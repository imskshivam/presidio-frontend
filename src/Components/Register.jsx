import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "../App.css";
import Loader from "./Loader";
import Snackbar from "./Snackbar";

const Register = () => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    phone: "",
    email: "",
    password: "",
    repeatPassword: "",
  });
  const [loading, setLoading] = useState(false);
  const [snackbar, setSnackbar] = useState({ message: "", isError: false });
  const navigate = useNavigate();

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleCloseSnackbar = () => {
    setSnackbar({ message: "", isError: false });
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);
      const response = await fetch(
        "http://13.234.37.152/auth/register-email",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ email: formData.email }),
        }
      );

      setLoading(false);
      const responseData = await response.json();

      if (response.ok) {
        setSnackbar({ message: responseData.message, isError: false });
        const { hash } = responseData.data;
        navigate("/OTP-Verification", { state: { ...formData, hash } });
      } else {
        setSnackbar({ message: responseData.message, isError: true });
      }
    } catch (error) {
      console.error("Error:", error);
      setSnackbar({ message: "Error occurred", isError: true });
    }
  };

  return (
    <div className="container">
      <form className="form" onSubmit={handleRegister}>
        <h2 className="title">Register User😁</h2>
        <input
          className="input"
          placeholder="First Name"
          name="firstName"
          value={formData.firstName}
          onChange={handleChange}
          required
        />
        <input
          className="input"
          placeholder="Last Name"
          name="lastName"
          value={formData.lastName}
          onChange={handleChange}
          required
        />
        <input
          className="input"
          placeholder="Phone Number"
          name="phone"
          value={formData.phone}
          onChange={handleChange}
          required
        />
        <input
          type="email"
          className="input"
          placeholder="Email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          required
        />
        <input
          type="password"
          className="input"
          placeholder="Password"
          name="password"
          value={formData.password}
          onChange={handleChange}
          required
        />
        <input
          type="password"
          className="input"
          placeholder="Repeat Password"
          name="repeatPassword"
          value={formData.repeatPassword}
          onChange={handleChange}
          required
        />
        {loading ? (
          <Loader />
        ) : (
          <button type="submit" className="button">
            Register
          </button>
        )}
      </form>
      {snackbar.message && (
        <Snackbar
          message={snackbar.message}
          isError={snackbar.isError}
          onClose={handleCloseSnackbar}
        />
      )}
    </div>
  );
};

export default Register;
