// src/components/Login.js

import React, { useState } from "react";
import Loader from "./Loader";
import "../App.css";
import { useNavigate } from "react-router-dom";
import Snackbar from "./Snackbar";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [snackbar, setSnackbar] = useState({ message: "", isError: false });
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const handleCloseSnackbar = () => {
    setSnackbar({ message: "", isError: false });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log({ email, password });

    try {
      setLoading(true);

      const response = await fetch("http://13.234.37.152/auth/email-login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          email,
          password,
        }),
      });

      setLoading(false);
      const responseData = await response.json();
      if (response.ok) {
        setSnackbar({ message: responseData.message, isError: false });
        console.log(responseData);
        localStorage.setItem("user", JSON.stringify(responseData));
        navigate("/");
      } else {
        setSnackbar({ message: responseData.message, isError: true });
      }
    } catch (error) {
      console.error("Error:", error);
      setSnackbar({ message: "Error occurred", isError: true });
    }
  };

  const handleRegisterClick = () => {
    navigate("/register");
  };

  return (
    <div className="container">
      <form className="form" onSubmit={handleSubmit}>
        <h2 className="title">Login</h2>
        <input
          type="email"
          className="input"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <input
          type="password"
          className="input"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
        {loading ? (
          <Loader />
        ) : (
          <button type="submit" className="button">
            Login
          </button>
        )}
      </form>

      {snackbar.message && (
        <Snackbar
          message={snackbar.message}
          isError={snackbar.isError}
          onClose={handleCloseSnackbar}
        />
      )}
      <div className="register-link">
        <p>
          Don't have an account?{" "}
          <button className="register-button" onClick={handleRegisterClick}>
            Register as a new user
          </button>
        </p>
      </div>
    </div>
  );
};

export default Login;
