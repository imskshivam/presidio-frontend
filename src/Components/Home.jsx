import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import SearchBar from "./SearchBar";
import PropertyList from "./PropertyList";
import Loader from "./Loader";
import "../App.css";

const Home = () => {
  const [properties, setProperties] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [query, setQuery] = useState("");

  const [user, setUser] = useState(null);

  const fetchProperties = async () => {
    setLoading(true);
    const res = await fetch(
      `http://13.234.37.152/properties?page=${page}&query=${query}`
    );
    const data = await res.json();

    // Ensure no duplicates are added
    setProperties((prevProperties) => {
      const newProperties = data.data.filter(
        (property) =>
          !prevProperties.some(
            (prevProperty) => prevProperty.id === property.id
          )
      );
      return [...prevProperties, ...newProperties];
    });

    setLoading(false);
  };

  useEffect(() => {
    fetchProperties();
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
      console.log();
    }
  }, [page, query]); // Fetch properties when page or query changes

  const handelInfiniteScroll = async () => {
    try {
      if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        setLoading(true);
        setPage((prev) => prev + 1);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handelInfiniteScroll);
    return () => window.removeEventListener("scroll", handelInfiniteScroll);
  }, []);

  const handleSearch = (query) => {
    setPage(1); // Reset to first page
    setProperties([]); // Clear current properties
    setQuery(query); // Set new search query
  };

  return (
    <div>
      <Navbar />
      <div className="home-container">
        <SearchBar onSearch={handleSearch} />
        <PropertyList
          properties={properties}
          isLogin={user == null ? false : true}
        />
        {loading && <Loader />}
      </div>
    </div>
  );
};

export default Home;
