import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "../App.css";

const Navbar = () => {
  const [user, setUser] = useState(null);
  const [menuActive, setMenuActive] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  const handleLogout = () => {
    localStorage.removeItem("user");
    setUser(null);
  };

  const handleNavigation = (path) => {
    if (user) {
      navigate(path);
    } else {
      navigate("/login");
    }
  };

  const toggleMenu = () => {
    setMenuActive(!menuActive);
  };

  return (
    <nav className="navbar">
      <div className="navbar-left">
        <h2>Property-delar</h2>
      </div>
      <div className={`navbar-right ${menuActive ? 'active' : ''}`}>
        <span
          className="navbar-link"
          style={{ cursor: "pointer" }}
          onClick={() => handleNavigation("/property/my-post")}
        >
          My Property
        </span>
        <span
          className="navbar-link"
          style={{ cursor: "pointer" }}
          onClick={() => handleNavigation("/post-property")}
        >
          Post Property
        </span>
        {user ? (
          <span
            onClick={handleLogout}
            className="navbar-link"
            style={{ cursor: "pointer" }}
          >
            Logout
          </span>
        ) : (
          <Link
            to="/login"
            className="navbar-link"
            style={{ cursor: "pointer" }}
          >
            Login
          </Link>
        )}
      </div>
      <div className="hamburger" onClick={toggleMenu}>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </nav>
  );
};

export default Navbar;
