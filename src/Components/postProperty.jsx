import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../App.css";
import Loader from "./Loader";
import Snackbar from "./Snackbar";

const PostProperty = () => {
  const [formData, setFormData] = useState({
    name: "",
    description: "",
    price: "",
    userId: "",
    bedrooms: "",
    bathrooms: "",
    size: "",
    location: "",
  });
  const [photos, setPhotos] = useState([]);
  const [loading, setLoading] = useState(false);
  const [snackbar, setSnackbar] = useState({ message: "", isError: false });
  const [user, setUser] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      const parsedUser = JSON.parse(storedUser);
      setUser(parsedUser);
      setFormData((prevData) => ({ ...prevData, userId: parsedUser.data._id }));
    } else {
      navigate("/login");
    }
  }, [navigate]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handlePhotoChange = (e) => {
    setPhotos([...e.target.files]);
  };

  const handleCloseSnackbar = () => {
    setSnackbar({ message: "", isError: false });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const propertyData = new FormData();
    propertyData.append("name", formData.name);
    propertyData.append("description", formData.description);
    propertyData.append("price", formData.price);
    propertyData.append("userId", formData.userId);
    propertyData.append("bedrooms", formData.bedrooms);
    propertyData.append("bathrooms", formData.bathrooms);
    propertyData.append("size", formData.size);
    propertyData.append("location", formData.location);

    photos.forEach((photo, index) => {
      propertyData.append("file", photo, photo.name);
    });

    try {
      setLoading(true);
      const response = await fetch(
        "http://13.234.37.152/properties/create-properties",
        {
          method: "POST",
          body: propertyData,
        }
      );

      setLoading(false);
      const responseData = await response.json();

      if (response.ok) {
        setSnackbar({ message: responseData.message, isError: false });
      } else {
        setSnackbar({ message: responseData.message, isError: true });
      }
    } catch (error) {
      console.error("Error:", error);
      setSnackbar({ message: "Error occurred", isError: true });
    }
  };

  return (
    <div className="container">
      <form className="form" onSubmit={handleSubmit}>
        <h2 className="title">Post Property</h2>
        <input
          className="input"
          placeholder="Name"
          name="name"
          value={formData.name}
          onChange={handleChange}
          required
        />
        <textarea
          className="input"
          placeholder="Description"
          name="description"
          value={formData.description}
          onChange={handleChange}
          required
        />
        <input
          type="number"
          className="input"
          placeholder="Price"
          name="price"
          value={formData.price}
          onChange={handleChange}
          required
        />
        <input
          type="number"
          className="input"
          placeholder="Bedrooms"
          name="bedrooms"
          value={formData.bedrooms}
          onChange={handleChange}
          required
        />
        <input
          type="number"
          className="input"
          placeholder="Bathrooms"
          name="bathrooms"
          value={formData.bathrooms}
          onChange={handleChange}
          required
        />
        <input
          type="number"
          className="input"
          placeholder="Size (sqft)"
          name="size"
          value={formData.size}
          onChange={handleChange}
          required
        />
        <input
          className="input"
          placeholder="Location"
          name="location"
          value={formData.location}
          onChange={handleChange}
          required
        />
        <input
          type="file"
          className="input"
          onChange={handlePhotoChange}
          multiple
          required
        />
        {loading ? (
          <Loader />
        ) : (
          <button type="submit" className="button">
            Post Property
          </button>
        )}
      </form>
      {snackbar.message && (
        <Snackbar
          message={snackbar.message}
          isError={snackbar.isError}
          onClose={handleCloseSnackbar}
        />
      )}
    </div>
  );
};

export default PostProperty;
