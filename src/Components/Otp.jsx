import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import "../App.css";
import { Link, useNavigate } from "react-router-dom";
import Loader from "./Loader";

const Otp = () => {
  const location = useLocation();
  const { firstName, lastName, phone, email, password, hash } = location.state;
  const [enteredOtp, setEnteredOtp] = useState("");
  const [loading, setLoading] = useState(false);
  const [snackbar, setSnackbar] = useState({ message: "", isError: false });
  const navigate = useNavigate();

  useEffect(() => {
    setSnackbar({ message: "OTP- sended to your Email", isError: false });
  }, []);

  const verifyOtp = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);

      // Handle post OTP verification logic here
      const response = await fetch(
        "http://13.234.37.152/auth/verify-otp-email",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            firstName,
            lastName,
            phone,
            email,
            password,
            otp: enteredOtp,
            hash: hash,
          }),
        }
      );

      setLoading(false);

      const responseData = await response.json();

      if (response.ok) {
        alert("User registered successfully");
        localStorage.setItem("user", JSON.stringify(responseData));
        navigate("/");
      } else {
        alert("Failed to verify OTP");
      }
    } catch (error) {
      setLoading(false);
      console.error("Error:", error);
    }
  };

  return (
    <div className="container">
      <form className="form" onSubmit={verifyOtp}>
        <h2 className="title">Verify OTP</h2>
        <input
          type="text"
          className="input"
          placeholder="Enter OTP"
          value={enteredOtp}
          onChange={(e) => setEnteredOtp(e.target.value)}
          required
        />
        {loading ? (
          <Loader />
        ) : (
          <button type="submit" className="button">
            Verify OTP
          </button>
        )}
      </form>
    </div>
  );
};

export default Otp;
