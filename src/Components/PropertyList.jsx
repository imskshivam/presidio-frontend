import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "../App.css";

const PropertyList = ({ properties, isSeller = false, isLogin = false }) => {
  const navigate = useNavigate();
  const [like, setLike] = useState(false);

  const handleCardClick = (propertyId) => {
    if (isLogin) {
      navigate(`/property/${propertyId}`);
    } else {
      navigate(`/Login`);
    }
  };

  const handleLike = async (propertyId) => {
    try {
      const user = JSON.parse(localStorage.getItem("user"));
      setLike(!like);
      if (!user) {
        console.error("User details not found.");
        return;
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <div className="property-list">
      {properties.map((property, index) => (
        <div
          key={property._id}
          className="property-card"
          style={{ cursor: "pointer" }}
          onClick={() => handleCardClick(property._id)}
        >
          <div className="image-container">
            <img
              src={property.images[0]}
              alt={property.name}
              className="property-image"
            />
            <button
              onClick={(e) => {
                e.stopPropagation(); // Prevent card click
                handleLike(property._id);
              }}
              className="like-button"
            >
              {like ? "❤️" : "🤍"}
            </button>
          </div>
          <div className="property-details">
            <h3 className="property-name">{property.name}</h3>
            <p className="property-price">${property.price}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default PropertyList;
