import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { FaBed, FaBath, FaRulerCombined, FaMapMarkerAlt } from "react-icons/fa";
import "../Styles/propertyDetail.css";

const PropertyDetail = () => {
  const { propertyId } = useParams();
  const [property, setProperty] = useState(null);
  const [user, setUser] = useState(null);
  const [seller, setSeller] = useState(null);
  const [requestSent, setRequestSent] = useState(false);

  useEffect(() => {
    const fetchProperty = async () => {
      try {
        // Fetch property details
        const propertyResponse = await fetch(
          `http://13.234.37.152/properties/getbyId?id=${propertyId}`
        );
        const propertyData = await propertyResponse.json();
        setProperty(propertyData.data);

        // Fetch seller details
        const sellerResponse = await fetch(
          `http://13.234.37.152/user/${propertyData.data.userId}`
        );
        const sellerData = await sellerResponse.json();
        setSeller(sellerData.data);
      } catch (error) {
        console.error("Error fetching property and seller details:", error);
      }
    };

    fetchProperty();
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, [propertyId]);

  const handleInterested = async () => {
    try {
      if (!user) {
        console.error("User details not found.");
        return;
      }

      const response = await fetch(
        `http://13.234.37.152/properties/intrested`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            // Authorization: `Bearer ${user.token}`,
          },
          body: JSON.stringify({
            senderEmail: user.data.email,
            recieverEmail: seller.email,
            name: user.data.first_name + " " + user.data.last_name,
            phone: user.data.phoneNumber,
            property_name: property.name,
            property_Id: propertyId,
          }),
        }
      );

      if (response.ok) {
        console.log("Marked as interested successfully.");
        setRequestSent(true);
      } else {
        console.error("Failed to mark as interested.");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  if (!property) {
    return <div>Loading...</div>;
  }

  return (
    <div className="property-detail-container">
      <div className="property-image-container">
        <img
          src={property.images[0]}
          alt={property.name}
          className="property-image"
        />
      </div>
      <div className="property-info">
        <div className="property-name-box">
          <h2 className="property-name">Property Name: {property.name}</h2>
        </div>
        <div className="property-description-box">
          <p className="property-description">{property.description}</p>
        </div>
        <div className="additional-attributes">
          <p><FaBed /> Bedrooms: {property.bedrooms}</p>
          <p><FaBath /> Bathrooms: {property.bathrooms}</p>
          <p><FaRulerCombined /> Size: {property.size} sq ft</p>
          <p><FaMapMarkerAlt /> Location: {property.location}</p>
        </div>
        <div className="property-price-box">
          <p className="property-price">${property.price}</p>
        </div>
        <button
          onClick={handleInterested}
          className={`interested-button ${requestSent ? "interested-sent" : ""}`}
          disabled={requestSent}
        >
          {requestSent ? "Request Sent" : "Interested"}
        </button>
      </div>
      {seller && (
        <div className="seller-details">
          <h3>Seller Details</h3>
          <p>
            Name: {seller.first_name} {seller.last_name}
          </p>
          <p>Email: {seller.email}</p>
          <p>Phone number: {seller.phoneNumber}</p>
          <p>Created at: {new Date(seller.createdAt).toLocaleDateString()}</p>
        </div>
      )}
    </div>
  );
};

export default PropertyDetail;
