import React, { useEffect } from "react";
import "../Styles/Snackbar.css";

const Snackbar = ({ message, isError, onClose }) => {
  useEffect(() => {
    const timeout = setTimeout(() => {
      onClose();
    }, 3000);

    return () => clearTimeout(timeout);
  }, [onClose]);

  return (
    <div className={`snackbar ${isError ? "error" : "success"}`}>{message}</div>
  );
};

export default Snackbar;
