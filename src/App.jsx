import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import Register from "./Components/Register";
import Login from "./Components/Login";
import Home from "./Components/Home";
import Otp from "./Components/Otp";
import PostProperty from "./Components/postProperty";
import PropertyDetail from "./Components/propertyDetail";

import Myproperties from "./Components/mypost";
import EditProperty from "./Components/EditProperty";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/register" element={<Register />} />

        <Route path="/login" element={<Login />} />

        <Route path="/" element={<Home />} />

        <Route path="/post-property" element={<PostProperty />} />

        <Route path="/OTP-Verification" element={<Otp />} />

        <Route path="/property/:propertyId" element={<PropertyDetail />} />

        <Route path="/property/edit/:propertyId" element={<EditProperty />} />

  

        <Route path="/property/my-post" element={<Myproperties />} />
      </Routes>
    </Router>
  );
}

export default App;
